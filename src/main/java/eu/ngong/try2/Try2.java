package eu.ngong.try2;

import static eu.ngong.util.Util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("/jour")
public class Try2 {
	private final static Logger l = LoggerFactory.getLogger(Try2.class);
	
	public Try2() {
		l.info("eu.ngong.try2.Try2 instantiated");
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String direBonjour(@QueryParam("p") String p) {
		l.debug(f("jour service, parameter p=%s",p));
		return "{\"name\":\"greeting: " + p + "\", \"message\":\"Bonjour tout le monde!\"}";	}
}
